# layui-icon

#### 介绍
基于layui 2.8.x 的图标选择器

#### 使用说明

1.引入
```
//...引入相关的layui js 及 图标js
<script src="icon.js"></script>
```
```
<div id="test"></div>
layui.use(['icon'], function () {
    let icon = layui.icon;
    
    icon.init({
        el: $('#test'),
        on: function (obj) {
            console.log(obj);
        }
    })
})
```
![image.png](image%2Fimage.png)
